package club.jsil.esf;

import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import db.Database;
import user.UserDB;

public class UserList extends AppCompatActivity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserDB userDB = new UserDB(getBaseContext());
        final Cursor cursor = userDB.getAll();

        String[] fields = new String[]{
                Database.USER_CNS,
                Database.USER_NOME,
                Database.USER_ADM,
                Database.USER_USERNAME,
                Database.USER_PASSWORD
        };
        int[] idsViews = new int[]{
                R.id.userListCns,
                R.id.userListNome,
                R.id.userListAdm,
                R.id.userListUsername,
                R.id.userListPassword
        };

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(getBaseContext(), R.layout.user_list_view, cursor, fields, idsViews, 0);

        listView = findViewById(R.id.userList);
        listView.setAdapter(adapter);

        TextView warning = findViewById(R.id.warning);

        if(cursor.getCount() == 0){
            warning.setText("Nenhum registro encontrado");
            warning.setVisibility(View.VISIBLE);
        } else {
            warning.setText("");
            warning.setVisibility(View.INVISIBLE);
        }

        FloatingActionButton fabAddUser = (FloatingActionButton) findViewById(R.id.fabAddUser);
        fabAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AddEditUser.class);
                startActivity(intent);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), AddEditUser.class);
                intent.putExtra("user_id", Long.toString(id));
                startActivity(intent);
            }
        });
    }
}
