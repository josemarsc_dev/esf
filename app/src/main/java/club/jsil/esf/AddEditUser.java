package club.jsil.esf;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import db.Database;
import user.User;
import user.UserDB;

public class AddEditUser extends AppCompatActivity {

    private static int USER_ID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_user);

        Button saveButton = findViewById(R.id.saveUserBtn);

        EditText cnsDef = (EditText) findViewById(R.id.cnsUser);
        EditText nomeDef = (EditText) findViewById(R.id.nomeUser);
        Switch admDef = (Switch) findViewById(R.id.admUser);
        EditText usernameDef = (EditText) findViewById(R.id.usernameUser);
        EditText passwordDef = (EditText) findViewById(R.id.passwordUser);

        LinearLayout linearLayout = findViewById(R.id.addEditUserButtonGroup);

        try {
            int id_user = Integer.parseInt(getIntent().getExtras().getString("user_id"));

            final UserDB userDB = new UserDB(getBaseContext());
            final User user = userDB.getById(id_user);

            USER_ID = user.getId();
            cnsDef.setText(user.getCns());
            nomeDef.setText(user.getNome());
            admDef.setChecked(user.getAdm().equals("adm") ? true : false);
            usernameDef.setText(user.getUsername());
            passwordDef.setText(user.getPassword());

            Button deleteButton = new Button(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            layoutParams.weight = 1;
            deleteButton.setLayoutParams(layoutParams);
            deleteButton.setText("Delete");
            deleteButton.setBackgroundResource(R.color.colorWarning);
            linearLayout.addView(deleteButton);

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int deleted = userDB.delete(USER_ID);
                    Toast.makeText(getApplicationContext(), "DELETED:" + Integer.toString(deleted), Toast.LENGTH_SHORT).show();
                    finish();
                }
            });


        } catch (Exception ex) {
            linearLayout.setWeightSum(1);
            USER_ID = 0;
            cnsDef.setText("10");
            nomeDef.setText("dez");
            admDef.setChecked(false);
            usernameDef.setText("dez");
            passwordDef.setText("dez");
        }

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserDB userDB = new UserDB(getBaseContext());

                EditText cns = findViewById(R.id.cnsUser);
                EditText nome = findViewById(R.id.nomeUser);
                Switch adm = findViewById(R.id.admUser);
                EditText username = findViewById(R.id.usernameUser);
                EditText password = findViewById(R.id.passwordUser);

                User user = new User();
                user.setId(USER_ID);
                user.setCns(cns.getText().toString());
                user.setNome(nome.getText().toString());
                user.setAdm((adm.isChecked() == true ? "adm" : "user"));
                user.setUsername(username.getText().toString());
                user.setPassword(password.getText().toString());

                int result;
                result = userDB.insert_update(user);
                String message = "";
                switch (result) {
                    case -1:
                        message = "Erro desconhecido salvar registro";
                        break;
                    case -1234:
                        message = "Erro de SQL";
                        break;
                    case 2067:
                        message = "CNS ou username fornecido já existe";
                        break;
                    default:
                        message = "Registro salvo com sucesso";
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                }

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
