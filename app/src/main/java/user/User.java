package user;

public class User {
    private int id;
    private String cns, nome, username, password, adm;

    public User() { }

    public User(int id, String cns, String nome, String adm, String username, String password) {
        this.id = id;
        this.cns = cns;
        this.nome = nome;
        this.adm = adm;
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCns() {
        return cns;
    }

    public void setCns(String cns) {
        this.cns = cns;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAdm() {
        return adm;
    }

    public void setAdm(String adm) { this.adm= adm; }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
